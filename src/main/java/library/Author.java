package library;

/**
 * This class models an author in our library context.
 * 
 */
public class Author implements Cloneable {

	/**
	 * Builds an author given its social security number
	 * 
	 * @param SSN: social security number 
	 */
	public Author(String SSN) {}
	
	/**
	 * Builds an author given its name, surname and SSN
	 * 
	 * @param name: 	optional, the name of the author, might be empty
	 * @param surname:	optional, the surname of the author, might be empty
	 * @param SSN: 		mandatory, social security number, cannot be empty
	 * 
	 * @throws IllegalArgumentException in case parameters are not valid e.g., are null.
	 */
	public Author(String name, String surname, String SSN) {}

	/**
	 * @return the authors name
	 * */
	public String getAuthorName() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the authors surname
	 * */
	public String getAuthorSurname() {
		throw new UnsupportedOperationException("");	
	}
	
	/**
	 * @return the authors SSN
	 * */
	public String getAuthorSSN() {
		throw new UnsupportedOperationException("");
	}
}
