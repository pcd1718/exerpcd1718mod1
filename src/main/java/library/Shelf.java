package library;

import java.util.ArrayList;

/**
 * This class models a shelf in our library context
 * 
 * */
public final class Shelf implements Cloneable {
	
	/**
	 * Builds a shelf of a certain capacity as specified by the input parameter
	 * 
	 * @param capacity: positive value denoting the maximum number of books the shelf can contain
	 * */
	public Shelf(int capacity) {}
	
	/**
	 * Builds a shelf of a certain capacity and a certain number of books
	 * 
	 * @param capacity: capacity of this shelf
	 * @param books:	books to store in this shelf
	 * 
	 * @throws IllegalArgumentException: in case parameters are not valid i.e., capacity not positive or capacity exceeded
	 * */
	public Shelf(int capacity, ArrayList<Book> books) {}


	/**
	 * Adds the specified book in the shelf
	 * 
	 * @param b: book to add
	 * 
	 * @return true if the book was added in the shelf, false otherwise
	 */
	public boolean addBookInShelf(Book b) {
		throw new UnsupportedOperationException("");
	}

	/**
	 * Adds the books specified in the parameter to this shelf
	 * 
	 * @param book:	an array of books to add to this shelf
	 * 
	 * @return the effective number of books added to this shelf
	 */
	public int addBooksInSheld(Book[] book) {
		throw new UnsupportedOperationException("");
	}


	/**
	 * @return the shelf capacity
	 * */
	public int getShelfCapacity() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return a list containing the books stored in this shelf
	 * */
	public ArrayList<Book> getBooksInShelf() {
		throw new UnsupportedOperationException("");
	}

	/**
	 * @return the number of books stored in this shelf
	 */
	public int getNumerOfBooksInShelf() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return true if the shelf is full (cannot contain any more books), false otherwise
	 * */
	public boolean shelfFull() {
		throw new UnsupportedOperationException("");
	}
}
