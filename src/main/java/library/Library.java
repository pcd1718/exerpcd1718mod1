package library;

import java.util.ArrayList;

/**
 * This class models the library which is comprised of several shelves holding books each of which identified by some parameters.
 */
public class Library {


	/**
	 * Builds an empty library
	 * */
	public Library() {}

	/**
	 * Builds a library given some shelves as a input parameter.
	 * 
	 * @param library: shelves to be added to this library
	 * 
	 * */
	public Library(ArrayList<Shelf> library) {}
	
	/**
	 * Adds a shelf to the library
	 * 
	 * @param shelf: the shelf
	 */
	public void addShelf(Shelf shelf) {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the total number of books present in this library
	 */
	public int getNumberOfBooksInLibrary() {
		throw new UnsupportedOperationException("");
	}

	/**
	 * @return the book year range (min, max) present in this library.
	 * E.g., if the oldest book year is 1200 and a newly added in 2017, the method returns an array of int {1200, 2017}
	 * */
	public int[] getBooksYearRangeInLibrary() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the average number of books stored in the library shelves.
	 * E.g., Shelf_1 contains 1 and Shelf_1 contains 2 contains 2 => Avg = (1+2)/2
	 */
	public double getAvgNumberOfBookPerShelf() {
		throw new UnsupportedOperationException("");
	}

	/**
	 * @return the number of distinct authors present in the library
	 * */
	public int getTotalNumberOfDistinctAuthorsInLibrary() {
		throw new UnsupportedOperationException("");
	}

	/**
	 * @return the total number of book pages present in this library
	 * */
	public int getTotalNumberOfBookPagesInLibrary() {
		throw new UnsupportedOperationException("");
	}
}
