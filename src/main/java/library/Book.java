package library;

import java.util.ArrayList;

/**
 * This class models a book in our library context
 * 
 * */
public class Book implements Cloneable {


	/**
	 * Sole class constructor.
	 * 
	 * @param title: 	the book title
	 * @param ISBN:  	the book ISBN identifier
	 * @param year:  	the year the book was published
	 * @param pages: 	the number of pages in the book
	 * @param authors:	a list of authors who wrote the book
	 * 
	 * @throws IllegalArgumentException: in case parameters are not valid
	 */
	public Book(String title, String ISBN, int year, int pages, ArrayList<Author> authors) {
	}
	
	/**
	 * @return the book title
	 * */
	public String getBookTitle() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the book ISBN identifier
	 * */
	public String getBookISBN() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the book year
	 * */
	public int getBookYear() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the book pages
	 * */
	public int getBookPages() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the book authors
	 * */
	public ArrayList<Author> getBookAuthors() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * @return the book author number
	 * */
	public int getBookAuthorNumber() {
		throw new UnsupportedOperationException("");
	}
	
	/**
	 * Adds an Author to this book
	 * 
	 * @param author: author to add
	 * */
	public void addBookAuthor(Author author) {
		throw new UnsupportedOperationException("");
	}
}